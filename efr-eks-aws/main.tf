# worker_groups = [
#  {
#    name                          = "worker-group-1"
#    instance_type                 = "t2.small"
#    additional_userdata           = "echo foo bar"
#    additional_security_group_ids = [aws_security_group.worker_group_mgmt_one.id]
#    asg_desired_capacity          = 2
#  },
#  {
#    name                          = "worker-group-2"
#    instance_type                 = "t2.medium"
#    additional_userdata           = "echo foo bar"
#    additional_security_group_ids = [aws_security_group.worker_group_mgmt_two.id]
#    asg_desired_capacity          = 1
#  },
# ]

module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  version         = "17.24.0"
  cluster_name    = local.cluster_name
  cluster_version = "1.20"
  subnets         = module.vpc.private_subnets

  vpc_id = module.vpc.vpc_id

  workers_group_defaults = {
    root_volume_type = "gp2"
  }

  worker_groups = [
    {
      name                          = "worker-group-1"
      instance_type                 = "t2.small"
      additional_userdata           = "echo foo bar"
      additional_security_group_ids = [aws_security_group.worker_group_mgmt_one.id]
      asg_desired_capacity          = 2
    },
    {
      name                          = "worker-group-2"
      instance_type                 = "t2.medium"
      additional_userdata           = "echo foo bar"
      additional_security_group_ids = [aws_security_group.worker_group_mgmt_two.id]
      asg_desired_capacity          = 1
    },
  ]
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}



# provider "aws" {
# #  version = "~> 2.57.0"
#   region = "eu-west-1"
# }

# terraform {
#   # required_version =  ">=0.13, <0.14" 

#   backend "s3" {
#     bucket  = "assessment-najm"
#     key     = "terraform.tfstate"
#     region  = "eu-west-1"
#     encrypt = "true"
#   }
# }

# locals {
#   cluster_name = "my-eks-cluster"
# }

# module "vpc" {
#   source = "git::https://git@github.com/reactiveops/terraform-vpc.git?ref=v5.0.1"

#   aws_region = "eu-west-1"
#   az_count   = 3
#   aws_azs    = "eu-west-1a, eu-west-1b, eu-west-1c"

#   global_tags = {
#     "kubernetes.io/cluster/${local.cluster_name}" = "shared"
#   }
# }

# module "eks" {
#   source       = "git::https://github.com/terraform-aws-modules/terraform-aws-eks.git?ref=v12.1.0"
#   cluster_name = local.cluster_name
#   vpc_id       = module.vpc.aws_vpc_id
#   subnets      = module.vpc.aws_subnet_private_prod_ids

#   node_groups = {
#     eks_nodes = {
#       desired_capacity = 3
#       max_capacity     = 3
#       min_capaicty     = 3

#       instance_type = "t2.small"
#     }
#   }

#   manage_aws_auth = false
# }

# module "eks" {
#   source          = "git::https://github.com/terraform-aws-modules/terraform-aws-eks.git?ref=v12.1.0"
#   cluster_name    = "test"
#   vpc_id          = "vpc-082b748152ad274a8"
#   subnets         = ["subnet-01bd3083aa7d76f35"]
#   cluster_version = "1.20"

#   node_groups = {
#     eks_nodes = {
#       desired_capacity = 3
#       max_capacity     = 4
#       min_capaicty     = 1
#       instance_type    = "t2.small"

#       k8s_labels = {
#         Environment = "StagingCluster"
#       }

#       # tags = {
#       #   Name = "staging"
#       # }
#     }
#   }

#   manage_aws_auth = false
#    map_users    = var.map_users
#   #  map_accounts = var.map_accounts
#   # write_kubeconfig = false 


#   tags = {
#     Environment = "StagingCluster"
#     Name = "staging"
#     Managedby = "Terraform"
#   }
# }

# output "cluster_endpoint" {
#   description = "Endpoint for EKS control plane."
#   value       = module.eks.cluster_endpoint
# }